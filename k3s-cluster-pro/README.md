# k3s-cluster-pro in Hetzner-Cloud with Terraform

_This project (testing in ubuntu 18.04) have tools for production how ElasticSearch,Kibana,Fluentd,ArgoCD,Prometheus,Istio,Jaeger,Kiali,Grafana... Too if you want integration with Gitlab and permission for RBAC below is repository, files and steps of configurations_

## Starting 🚀

_Follow these instructions. Remember apply this commands in your machine master of cluster, except the requirements_


### Requirements 📋

_You should install Terraform, keys-ssh, unzip and git in your local-machine_

```
sudo apt update
```
```
sudo apt install git
```
```
sudo apt install unzip
```
```
ssh-keygen
```
```
wget https://releases.hashicorp.com/terraform/0.13.4/terraform_0.13.4_linux_amd64.zip
```
```
unzip terraform_0.13.4_linux_amd64.zip
```
```
mv terraform /usr/local/bin
```
```
terraform version
```

## Clone Repository 📄

```
https://gitlab.com/daicarjim/k3s-cluster-pro.git
```
_Access to directory_

```
cd k3s-cluster-pro
```

### Create Hetzner Token 📖

_To provision the infrastructure where k3s-cluster-pro will be installed, it is necessary to have an account in Hetzner Cloud. Once we have the account, we must give Terraform access so that it can create the infrastructure for us.
For this it is necessary to generate a token, go to the page: https://colinwilson.uk/2020/10/31/generate-an-api-token-in-hetzner-cloud/ this is a short-tutorial and create it._


## Settings ⚙️

_Create file terraform.tfvars and paste variable of token_

```
vim terraform.tfvars
```

_Paste and configure_

```
hcloud_token = "YOUR-TOKEN-HETZNER"
```

### Creation and instalation of cluster 🔧

_Execute_

```
terraform init
```
```
terraform plan
```
```
terraform apply
```
_Please before the access to cluster wait 5 minutes because bash is executing scripts_

_NOTE: Configure Load Balancer Target Manually (ip master)_

# Addional settings (optional)⚙️

_Access to Management Machine (Master)_

```
ssh -p22 root@IP-Management-Node
```

## Expose Service EKF (ElasticSearch,Kibana,Fluentbit/Fluentd)⚙️


_1) CHANGE VARIABLE (ClusteIP) FOR (NodePort)_

```
kubectl edit service/quickstart-kb-http -n logging
```

_2) (copy-port 30.... example: IP-MASTER-OR-WORKER:30987)_

```
kubectl get svc -n logging
```

_3) Search in navigator https://IP-Management-Node:copy-port (example https://192.87.65.74:30987)_


_4) Copy password for access to Kibana in the browser. Remember that your user is: elastic_

```
kubectl get secret quickstart-es-elastic-user -n logging -o=jsonpath='{.data.elastic}' | base64 --decode; echo 
```


## Expose Service UI DASHBOARD KUBERNETES (With authetication TOKEN BEARER)⚙️


_1) CHANGE VARIABLE (ClusteIP) FOR (NodePort)_

```
kubectl edit service/kubernetes-dashboard -n kubernetes-dashboard
```

_2) (copy-port 30.... example: IP-MASTER-OR-WORKER:30987)_

```
kubectl get svc -n kubernetes-dashboard 
```

_3) Search in navigator https://IP-Management-Node:copy-port (example https://192.87.65.74:30987)_

_4)(copy password for access to UI-KUBERNETES)_

```
sudo k3s kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token 
```

## Generate access RBAC to developers in namespace development (testing)⚙️

_1) If you want to give access developers, in the repository exist the files with step <rbac-developer1.sh> <rbac-operator2.txt> and <rbac-developer3.sh> for the RBAC with openSSL._

_Or download with this commands:_

```
wget https://www.dropbox.com/s/x4dr21u5eoebv2l/rbac-developer1.sh
```
```
wget https://www.dropbox.com/s/aq4c0f3bx7gtso6/rbac-operator2.txt 
```
```
wget https://www.dropbox.com/s/he3fqnhc1qhqecl/rbac-developer3.sh
```

_NOTE: Before the execute the files .sh read them first because there are instructions_

## Access ARGOCD⚙️

_1) (Access to port 30... example: IP-MASTER-OR-WORKER:30987)_

```
kubectl get service argocd-server -n argocd
```

_2) (Get the password)(User is "admin" for default)_

```
kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2
```

_3) If you dont can access then you can get the initial password_

```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo
```

## Create instance GITLAB on-premise⚙️

_Follow the steps of this respository:_

```
https://gitlab.com/daicarjim/gitlab-server-with-terraform-in-hetznercloud
```

## Connect your cluster k3s with GITLAB instance (on-premise):⚙️

_1) Already is create service account gitlab-admin in kube-system with ClusterRoleBinding and ClusterRole,the name of service account is "gitlab-admin", if you want verified with:_

```
kubectl get sa -n kube-system
```
_now you should have the token and CA for the connection with GITLAB instance on-premise:_

_2) With this have token. Copy TOKEN and save for the moment in other place_

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
```

_3) With this have CA (Certificate Authority)_

```
cat /etc/rancher/k3s/k3s.yaml
```

_4) Then copy your CA and convert value in base64 decode with the next command:_

```
echo (Copy here code Certificate Authority or CA) | base64 -d
```

_5) Now visit (the Gitlab intance on premise) the admin area for your cluster and click Kubernetes (or for a group the group settings page -> kubernetes and for a project the project -> operations -> kubernetes screen). Click the “Add Kubernetes Cluster” button and fill in the required information:_

_*Enter a name for your cluster_

_*Enter the API URL. You can simply use the IP address of the k3s system or if you’re being fancy the DNS name you may have supplied_

_*In CA Certificate enter in the decoded certificate value you got before_

_*In the Service Token enter in the token information you recored. Do not attempt to base64 decode this value._

_*When finished your screen will look similar to the screenshot below:_

```
https://blog.dustinrue.com/wp-content/uploads/2019/12/Screen-Shot-2019-12-24-at-9.39.58-PM-1024x596.png
```

## If you want install and configure ISTIO,GRAFANA,PROMETHEUS,KIALI and JAEGER,copy this in your terminal master-machine:⚙️

```
wget https://www.dropbox.com/s/7wn4wvwpanja88r/istio.sh
```

```
chmod +x istio.sh
```

```
bash istio.sh
```

_You should see namespaces istio (istio-system and istio-rbac)_

```
kubectl get namespaces
```

_If you want access to Jaeger, Prometheus, Grafana and Kiali you should modified the service to NodePort._

```
kubectl edit service/tracing -n istio-system
```
```
kubectl edit service/prometheus -n istio-system
```
```
kubectl edit service/grafana -n istio-system
```
```
kubectl edit service/kiali -n istio-system
```

_Then you can see the NodePorts for access from the browser:_

```
kubectl get svc -n istio-system 
```


# GRATITUDE 🎁

* https://www.udemy.com/course/learn-devops-advanced-kubernetes-usage/ 
* https://www.udemy.com/course/kubernetes-de-principiante-a-experto/ 
* https://github.com/HoussemDellai/eck-fluentd-operators-kubernetes
* https://medium.com/@HoussemDellai/rbac-with-kubernetes-in-minikube-4deed658ea7b
* https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
* https://www.adictosaltrabajo.com/2020/05/25/implementando-gitops-con-argocd/
* https://www.digitalocean.com/community/tutorials/como-instalar-y-configurar-gitlab-en-ubuntu-18-04-es
* https://blog.dustinrue.com/2019/12/gitlab-k3s/
* https://computingforgeeks.com/how-to-secure-gitlab-server-with-ssl-certificate/
* https://docs.gitlab.com/omnibus/installation/
* https://istio.io/latest/docs/examples/microservices-istio/setup-kubernetes-cluster/
* https://istio.io/latest/docs/setup/getting-started/
* https://istio.io/latest/docs/setup/install/istioctl/
* https://betterprogramming.pub/getting-started-with-istio-on-kubernetes-e582800121ea
* https://www.youtube.com/watch?v=voAyroDb6xk
* https://github.com/cicdteam/terraform-hcloud-k3s
* https://betterprogramming.pub/k8s-tips-give-access-to-your-clusterwith-a-client-certificate-dfb3b71a76fe
---
⌨️ Esto se hizo con el ❤️ para la comunidad por [daicarjim](https://gitlab.com/daicarjim) 👍
