terraform {
  backend "http" {

     
  }
}

variable "hcloud_token" {
  type = string
}

provider "hcloud" {
  token = var.hcloud_token
}

# Create a new SSH key
resource "hcloud_ssh_key" "default" {
  name = "hetzner"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFasyPqKse31nuw+m3kW+RTFlTD2aUlXVLp9F11t5xHDk1KHZkaX7k6smSM7XFagysctxD44bcyTRyQlNJRpxp9oL1p4t1PWh3Wes3niO1jc5u29qti0nk4Ym/F05lO5U0FRUPD2ZIhQoetA4CosGGcSZzBQzalornW13VU37WgA/vNbFiyJcTwSm1ZEsCX48V3L/fiX8vAZB3CLxUyCnmAIwQOyKTRaOoC2Vn71pc9DKm5Ix1AMm66Hg8+gkLdj28vRze/8NfVjx5k0/Gz7R7GG65A1tGyoYMrI+rQxla3e5d/b8bUByMaO7Qn1JGHcHF+kqiXKw+sozFw8q5fxxZtCfA8R3CLZd93kLdNM38Rgt7oW5G7O+pu2Isa6AFrQcIy59shmgQnqxYv0yCVSYfrCXHE3iR0V6emZ3vVeYUVCOz8kdKVi6TXUiDghenf5gY224rpvMe4RUzXk7mamMMDdNlWVCVSZwhyZrWvDrH5Pd+ZvJLtb7CliNfg9BfjCs= daimo@DESKTOP-QGKQQB4"
}

module "cluster" {
  source  = "cicdteam/k3s/hcloud"
  version = "0.1.2"
  hcloud_token = var.hcloud_token
  ssh_keys = [hcloud_ssh_key.default.id]
  master_type = "cpx31"

  node_groups = {
       "cx31" = 1
  }

}

output "master_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the master node"
  value       = module.cluster.master_ipv4
}

output "nodes_ipv4" {
  depends_on  = [module.cluster]
  description = "Public IP Address of the worker nodes"
  value       = module.cluster.nodes_ipv4
}
